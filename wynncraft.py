'''
Wynncraft
=========

Claims a Free Lucky Crate off of store.wynncraft.com
Timeout: 1 Month
Working: 2019-09-22
'''


import argparse
from selenium import webdriver


parser = argparse.ArgumentParser(
    description='Claims a free lucky crate for a given player on the Wynncraft Minecraft Server'
)
parser.add_argument('player', action='store')

args = parser.parse_args()

browser = webdriver.Firefox()
browser.get('https://store.wynncraft.com/')

# Add Free Crate to Basket
'''
<div class="store-container content-container">
    <div class="store-inner container text-center">
        <a class="free-crate-container" href="/checkout/packages/add/2381832/single" data-original-title="" title=""><!-- ... --></a> <!-- xpath_crate -->
        <!-- ... -->
    </div>
</div>
'''

xpath_crate = '//div[@class="store-container content-container"]/div[@class="store-inner container text-center"]/a[@class="free-crate-container"]'
browser.find_element_by_xpath(xpath_crate).click()

# Send Playername
'''
<form method="post">
    <div class="username">
        <div class="input-group">
            <input type="text" name="ign" class="form-control input-lg"> <!-- xpath_ign -->
            <div class="input-group-btn"><button class="btn btn-success btn-lg" type="submit">Continue</button></div> <!-- xpath_continue -->
        </div>
    </div>
</form>
'''

xpath_ign = '//form[@method="post"]/div[@class="username"]/div[@class="input-group"]/input[@name="ign"]'
browser.find_element_by_xpath(xpath_ign).send_keys(args.player)

xpath_continue = '//form[@method="post"]/div[@class="username"]/div[@class="input-group"]/div[@class="input-group-btn"]/button[@type="submit"]'
browser.find_element_by_xpath(xpath_continue).click()

# Check if Item was added to Basket
if browser.current_url == 'https://store.wynncraft.com/':
    print(f'[Wynncraft] Unable to claim Free crate for {args.player}')
    browser.close()
    exit()

# Confirm Purchase
'''
<form class="gateway" method="post" action="/checkout/free">
    <!-- ... -->
    <div class="privacyStatement">
        <!-- ... -->
        <p>
            <input type="checkbox" name="privacyConsent" id="privacyConsent" value="1" required="required" oninvalid="clearWaitingOverlay()" style=""> <!-- privacyConsent -->
            <!-- ... -->
        </p>
        <!-- ... -->
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="true" name="agreement" style=""> Do you agree to the <a id="tos-btn" data-toggle="modal" href="#tos-modal">Terms of Service</a>? <!-- xpath_tos -->
                </label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <button type="submit" class="btn btn-success btn-block" data-loading-text="Successfully Claimed!">Claim for free »</button> <!-- xpath_checkout -->
            </div>
        </div>
    </div>
</form>
'''

browser.find_element_by_id('privacyConsent').click() # Agree tp Privacy Statement

# Agree to Terms of Service
xpath_tos = '//form[@class="gateway"]/div[@class="row"]/div[@class="col-sm-8"]/div[@class="checkbox"]/label[1]/input[@name="agreement"]'
browser.find_element_by_xpath(xpath_tos).click()

# Checkout
xpath_checkout = '//form[@class="gateway"]/div[@class="row"]/div[@class="col-sm-4"]/div[@class="form-group"]/button[@type="submit"]'
browser.find_element_by_xpath(xpath_checkout).click()

browser.close()

print(f'[Wynncraft] Free Crate Claimed for {args.player}')

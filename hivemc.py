'''
HiveMC
======

Claims a Free Lucky Crate off of store.hivemc.com
Timeout: 1 Month
Working: 2019-09-21
'''


import argparse
from selenium import webdriver
import time


parser = argparse.ArgumentParser(
    description='Claims a free lucky crate for a given player on the HiveMC Minecraft Server'
)
parser.add_argument('player', action='store')

args = parser.parse_args()

browser = webdriver.Firefox()
browser.get('https://store.hivemc.com/category/724490')

# Send Playername
'''
<form method="post">
    <div class="main-form">
        <!-- ... -->
        <input type="text" placeholder="Enter here..." name="ign"> <!-- xpath_ign -->
        <!-- ... -->
    </div>
    <div class="actions">
        <input type="submit" value="Continue"> <!-- xpath_submit -->
    </div>
</form>
'''

xpath_ign = '//form[@method="post"]/div[@class="main-form"]/input[@name="ign"]'
browser.find_element_by_xpath(xpath_ign).send_keys(args.player)

xpath_continue = '//form[@method="post"]/div[@class="actions"]/input[@type="submit"]'
browser.find_element_by_xpath(xpath_continue).click()

# Add the Lucky Crate to the Cart
'''
<div class="right_heavy">
    <!-- ... -->
    <div class="package"> 
        <!-- ... -->
        <a href="#" class="hive-btn modal-btn" data-modal-package="2108501">Buy (0.00 EUR)</a> <!-- xpath_luckycrate -->
    </div>
</div>
'''

xpath_luckycrate = '//div[@class="right_heavy"]/div[@class="package"]/a[@class="hive-btn modal-btn" and @data-modal-package="2108501"]'
browser.find_element_by_xpath(xpath_luckycrate).click()
time.sleep(1)  # Wait for JavaScript to update the site

# "Add to Cart Button" -> Proceed to checkout
'''
<div id="modal-box">
    <!-- ... -->
    <div class="footer">
        <!-- ... -->
        <a href="/checkout/packages/add/2108501/single" class="hive-btn"><i class="fa fa-shopping-cart"></i> Add to cart</a> <!-- xpath_cart -->
    </div>
</div>
'''

# Check if Item can be added to cart
xpath_available = '//div[@id="modal-box"]/div[@class="footer"]/span[1]'
text = browser.find_element_by_xpath(xpath_available).get_attribute('innerHTML')
if text == 'You\'re not allowed to purchase this item.':
    print(f'[HiveMC] Unable to claim Free Lucky Crate for {args.player}')
    browser.close()
    exit()

xpath_cart = '//div[@id="modal-box"]/div[@class="footer"]/a[@class="hive-btn"]'
browser.find_element_by_xpath(xpath_cart).click()

# Confirm Purchase
'''
<div class="left_heavy">
    <!-- ... -->
    <div class="box">
        <!-- ... -->
        <div class="methods">
            <!-- ... -->
            <a href="#" title="Click to checkout for FREE!" onclick="HiveCheckout.doFree(); return false;"><!-- ... --></a> <!-- xpath_confirmpurchase -->
        </div>
    </div>
</div>
'''

browser.find_element_by_id('agreement').click()  # Accept Terms and Conditions
browser.find_element_by_id('privacyConsent').click()  # Accept Privacy Policy

xpath_confirmpurchase = '//div[@class="left_heavy"]/div[@class="box"]/div[@class="methods"]/a'
browser.find_element_by_xpath(xpath_confirmpurchase).click()

browser.close()

print(f'[HiveMC] Free Lucky Crate Claimed for {args.player}')
